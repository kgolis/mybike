# MyBike

###Last project on SDA course.###

Main idea is to have system like Nextbike for renting and returning bikes. 

Currently working parts are : 

* Logon and registeration
* Renting bikes
* Returning bikes

Future plans on improvement : 

* REST Api implementation
* Maps Api implementation
* Angular frontend
* Fixing bugs

###Project is using :###

* Spring Core
* Spring Data Jpa
* Spring Security
* JSP
* Hibernate
* Postgres SQL

### Project is written as Model View Controller. ###

###Project team:###
* Kamil Golis
* Kamil Adamczewski
* Patryk Kwiatkowski
* Jakub Frączek